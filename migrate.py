import requests, pprint, os

SRHT_TOKEN = os.getenv("SRHT_TOKEN")
GITEA_TOKEN = os.getenv("GITEA_TOKEN")

def main():
    gitea_headers = {"Authorization": f"token {GITEA_TOKEN}"}
    res = requests.get("https://git.augendre.info/api/v1/user", headers=gitea_headers)
    gitea_data = res.json()
    gitea_username = gitea_data["login"]
    res = requests.post(
        "https://git.sr.ht/query",
        headers={"Content-Type": "application/json", "Authorization": f"Bearer {SRHT_TOKEN}"},
        json={
            "query": """
            {
              me {
                canonicalName
                repositories(filter: {count: 35}) {
                  results {
                    name, description
                  }
                }
              }
            }""",
        }
    )
    srht_data = res.json()
    username = srht_data["data"]["me"]["canonicalName"]
    for repo in srht_data["data"]["me"]["repositories"]["results"]:
        name = repo["name"]
        description = repo["description"]
        url = f"https://git.sr.ht/{username}/{name}"

        gitea_data = {
          "clone_addr": url,
          "description": description,
          "issues": True,
          "labels": True,
          "milestones": True,
          "mirror": False,
          "private": False,
          "pull_requests": True,
          "releases": True,
          "repo_name": name,
          "repo_owner": gitea_username,
          "service": "git",
          "uid": 0,
          "wiki": True
        }
        res = requests.post("https://git.augendre.info/api/v1/repos/migrate", headers=gitea_headers, json=gitea_data)


if __name__ == "__main__":
    main()
