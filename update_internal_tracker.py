import requests, pprint, os

GITEA_TOKEN = os.getenv("GITEA_TOKEN")
BASE_URL = "https://git.augendre.info/api/v1"

def main():
    session = requests.Session()
    session.headers.update({"Authorization": f"token {GITEA_TOKEN}"})
    res = session.get(f"{BASE_URL}/user/repos", params={"limit": 40})
    repos = res.json()

    for repo in repos:
        full_name = repo["full_name"]
        url = f"{BASE_URL}/repos/{full_name}"
        res = session.patch(url, json={
            'has_issues': True,
            'internal_tracker': {'enable_time_tracker': False, 'allow_only_contributors_to_track_time': True, 'enable_issue_dependencies': True}
        })
        print(full_name, res.status_code)


if __name__ == "__main__":
    main()
