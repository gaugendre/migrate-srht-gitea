import requests, pprint, os
from tqdm import tqdm

GITEA_TOKEN = os.getenv("GITEA_TOKEN")
BASE_URL = "https://git.augendre.info/api/v1"

def main():
    session = requests.Session()
    session.headers.update({"Authorization": f"token {GITEA_TOKEN}"})
    res = session.get(f"{BASE_URL}/user/repos", params={"limit": 40})
    repos = res.json()
    missing_license = []

    for repo in tqdm(repos):
        full_name = repo["full_name"]
        url = f"{BASE_URL}/repos/{full_name}/raw/LICENSE"
        res = session.get(url)
        if res.status_code != 200:
            missing_license.append(repo["html_url"])

    print("missing license:")
    pprint.pprint(missing_license)


if __name__ == "__main__":
    main()
